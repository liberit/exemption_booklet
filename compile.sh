#!/bin/bash
lualatex exemption.tex
lualatex exemption.tex
pdfbook2 exemption.pdf --paper=letterpaper  --top-margin=10 --bottom-margin=5 --outer-margin=20 --signature=8 --inner-margin=80
pandox exemption.tex -o exemption.txt
