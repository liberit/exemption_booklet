2

Introduction
============

Freedom of Religion is protected by the Canadian Constitution, to which
the Re-opening Ontario Act is subject. I believe that God created all
that is, and one can find the spark of the divine in each belief system.
Herein, I explain my beliefs through the language and holy texts of
several major religions that God is a healer, and that we are to worship
God, the most merciful, most compassionate only, and not worldly
fear/anger mongering hierarchies. I along with billions of others have
been sent to incarnate on Earth at this time to help with the transition
to the New Earth, as the old one is passing away. Not all of us are
awake to our purpose here, but perhaps you will join us in co-creating
open hearted society of people cultivating forgiveness, compassion and
unconditional love for all beings. May you be blessed for a thousand
generations.

Faucism
=======

While Fauci is an American, unfortunately some Canadian politicians are
worshipping him as a God now, seeing him and his decrees as infallible
despite ample evidence to the contrary.

COVID fear is by Faith Alone
----------------------------

The government has never been able to prove in court that there was or
is a pandemic. The World Health Organization had to redefine "pandemic"
from "spread of a deadly disease" to "spread of a disease" because COVID
can not be considered deadly.

The Current Infection Fatality Rate for COVID is 0.15% (
https://onlinelibrary.wiley.com/doi/epdf/10.1111/eci.13554 ) by contrast
the All Cause Fatality rate in Canada in 2018 was 0.712%, in 2019 it was
0.776% in 2020 it was 0.780%. All time low for mortality was 0.69% in
1983 and has been steadily climbing due to population dynamics. UN
projects it will reach 1.05% mortality rate by 2050, due to boomers
reaching end of life expectancy.

It is 5 times higher risk to be alive for a year, than to get a COVID
infection. As a Canadian you have a 0.13% chance of dying in any 60 day
interval.

The worshipping of Fauci and his COVID cult is forbidden to any follower
of God. Fauci speaks many false prophecies, and dictates many sinister
rituals that hurt, maim, kill and destroy those led astray by him. Let
us review the cult of Faucism to have a better understanding of that it
is a form of idolatry.

Tenets of Faucism
-----------------

False Prophet Fauci ushers in an Age of Fear, and the first step to
entering the Pandemic is masking to conceal your sneer as you close your
heart, and fill yourself with fear of COVID and anger at the non
believers. The original sin in Faucism is to be born with a body lacking
genetic modifications, so you must take a holy communion of gene therapy
injections multiple times a year to rid you of your sin body. Glorify in
the blood clots, disability and death that ensues from the gene therapy
injections. Also fear the government and demand more oppression, become
a masochist for lockdowns, distancing and surveillance. Sacrifice your
family and friends on the alter of Faucism, glorify in their side
effects and deaths. Never accept any responsibility for anything bad
that happens, blame the unbelievers.

Did I miss anything or is that about it?

Statistically we know that COVID is a very safe virus with an extremely
low mortality rate, being afraid of it is purely based on putting ones
faith in the cult of Faucism.

Worshiping Faucism for the majority is akin to a neo-Molochian cult,
since it mostly consists of doing human sacrifices, via suffocating
self, friends and relatives, as well as killing them with poison
injections. Babies born in 2021 are showing a 22% IQ decline, likely as
a result of lower oxygen intake by mothers wearing masks, and the forced
masking of children under 5 who's brains are still expanding rapidly.
Wearing a mask while pregnant is similar to smoking while pregnant,
similarly putting masks on children is like smoking near children, in
terms of how much it lowers their effective oxygen consumption.

For the politicians and business people it is often a neo-mammon cult,
since they are being paid large amounts of money to follow along, and
their money supply is threatened if they do not.

Act in fear and you will get that which you fear. We know this as fact
from all the countries that have followed Faucism, they have the highest
number of people sick and dying with COVID in their DNA, mostly due to
the clotfactories that are now a part of them due to the genetic
injections. Though certainly the Fauci ritual \"Covid treatment\" of
Remdesivir with unconscious intubation certainly helps kill more people,
to further increase the number of human sacrifices for this
neo-Molochian cult.

Admittedly Rockefeller/pharmakeia medicine has been doing these human
sacrifices for almost a century now, for example how they have been
\"treating\" cancer patients with radiation/chemotherapy when we know
from countless studies that the cure for cacner is actually forgiveness
(following Jesus's commandments). But only recently have they gotten
enough worshippers that they are attempting to sacrifice the majority of
the human population to Moloch.

Instead I ask of thee to return to God by Loving God, so that we may get
more of that which we Love. Forgiveness, compassion and unconditional
love for all beings (all of God's creation), is the path back to God.

Judaism
=======

All bible quotes are from Young's Literal Translation unless otherwise
noted.

Straying from God is the cause of disease
-----------------------------------------

Exodus 20:3 'Thou hast no other Gods before Me.

Deuteronomy 8:19 'And it hath been -- if thou really forget Jehovah thy
God, and hast gone after other gods, and served them, and bowed thyself
to them, I have testified against you to-day that ye do utterly perish;

Deuteronomy 11:16-17 16 'Take heed to yourselves, lest your heart be
enticed, and ye have turned aside, and served other gods, and bowed
yourselves to them, 17 and the anger of Jehovah hath burned against you,
and He hath restrained the heavens, and there is no rain, and the ground
doth not give her increase, and ye have perished hastily from off the
good land which Jehovah is giving to you.

Isaiah 1:5 Wherefore are ye stricken any more? Ye do add apostacy! Every
head is become diseased, and every heart \[is\] sick.

2 Chronicles 24:18 and they forsake the house of Jehovah, God of their
fathers, and serve the shrines and the idols, and there is wrath upon
Judah and Jerusalem for this their guilt.

Psalm 106:36 And serve their idols, And they are to them for a snare.

Ezekiel 18:12-13 12 The afflicted and needy he hath oppressed, Plunder
he hath taken violently away, A pledge he doth not return, And unto the
idols he hath lifted up his eyes, Abomination he hath done! 13 In usury
he hath given, and increase taken, And he liveth: he doth not live, All
these abominations he hath done, He doth surely die, his blood is on
him.

Ezekiel 18:4 Lo, all the souls are Mine, As the soul of the father, So
also the soul of the son -- they are Mine, The soul that is sinning --
it doth die.

Ezekiel 20:8 And -- they rebel against Me, And have not been willing to
hearken to Me, Each, the detestable things of their eyes, They have not
cast away, And the idols of Egypt have not forsaken, And I say -- to
pour out My fury on them, To complete Mine anger against them,

God is the healer
-----------------

Deuteronomy 7:15 and Jehovah hath turned aside from thee every sickness,
and none of the evil diseases of Egypt (which thou hast known) doth He
put on thee, and He hath put them on all hating thee.

Psalm 103:2-3 2 Bless, O my soul, Jehovah, And forget not all His
benefits, 3 Who is forgiving all thine iniquities, Who is healing all
thy diseases,

Zechariah 13:2 And it hath come to pass, in that day, An affirmation of
Jehovah of Hosts, I cut off the names of the idols from the land, And
they are not remembered any more, And also the prophets and the spirit
of uncleanness I cause to pass away from the land.

Christianity
============

All bible quotes are from Young's Literal Translation unless otherwise
noted.

Straying from God is the cause of disease
-----------------------------------------

Matthew 4:10 Then saith Jesus to him, 'Go -- Adversary, for it hath been
written, The Lord thy God thou shalt bow to, and Him only thou shalt
serve.'

Matthew 6:22-24 22 'The lamp of the body is the eye, if, therefore,
thine eye may be perfect, all thy body shall be enlightened, 23 but if
thine eye may be evil, all thy body shall be dark; if, therefore, the
light that \[is\] in thee is darkness -- the darkness, how great! 24
'None is able to serve two lords, for either he will hate the one and
love the other, or he will hold to the one, and despise the other; ye
are not able to serve God and Mammon.

(mere lip service to Jesus is not enough:) Matthew 7:21 'Not every one
who is saying to me Lord, lord, shall come into the reign of the
heavens; but he who is doing the will of my Father who is in the
heavens.

John 12:39-40 39 Because of this they were not able to believe, that
again Isaiah said, 40 'He hath blinded their eyes, and hardened their
heart, that they might not see with the eyes, and understand with the
heart, and turn back, and I might heal them;'

Revelation 22:3 and any curse there shall not be any more, and the
throne of God and of the Lamb shall be in it, and His servants shall
serve Him,

God is a healer
---------------

Matthew 8:7 and Jesus saith to him, 'I, having come, will heal him.'

Matthew 8:13 And Jesus said to the centurion, 'Go, and as thou didst
believe let it be to thee;' and his young man was healed in that hour.

Matthew 10:1 he gave to them power over unclean spirits, so as to be
casting them out, and to be healing every sickness, and every malady.

Matthew 12:15 and Jesus having known, withdrew thence, and there
followed him great multitudes, and he healed them all,

Matthew 15:28 then answering, Jesus said to her, 'O woman, great \[is\]
thy faith, let it be to thee as thou wilt;' and her daughter was healed
from that hour.

Matthew 19:2 and great multitudes followed him, and he healed them
there.

James 5:14-16 14 is any infirm among you? let him call for the elders of
the assembly, and let them pray over him, having anointed him with oil,
in the name of the Lord, 15 and the prayer of the faith shall save the
distressed one, and the Lord shall raise him up, and if sins he may have
committed, they shall be forgiven to him. 16 Be confessing to one
another the trespasses, and be praying for one another, that ye may be
healed; very strong is a working supplication of a righteous man;

Follow Jeusus's Commands to be healed
-------------------------------------

Matthew 6:14 'For, if ye may forgive men their trespasses He also will
forgive you -- your Father who \[is\] in the heavens;

Matthew 22:37-39 37 And Jesus said to him, 'Thou shalt love the Lord thy
God with all thy heart, and with all thy soul, and with all thine
understanding -- 38 this is a first and great command; 39 and the second
\[is\] like to it, Thou shalt love thy neighbor as thyself;

John 13:34 'A new commandment I give to you, that ye love one another;
according as I did love you, that ye also love one another;

John 15:10 if my commandments ye may keep, ye shall remain in my love,
according as I the commands of my Father have kept, and do remain in His
love;

Matthew 28:19-20 19 having gone, then, disciple all the nations, 20
teaching them to observe all, whatever I did command you,) and lo, I am
with you all the days -- till the full end of the age.'

Islam
=====

Straying from God is the cause of disease
-----------------------------------------

Quotes from the Quran are from the Clear Quran translated by Talal Itani

Quran 5:60 Say, \"Shall I inform you of worse than that for retribution
from God? He whom God has cursed, and with whom He became angry; and He
turned some of them into apes, and swine, and idol worshipers. These are
in a worse position, and further away from the right way.\"

Quran 6:137 Likewise, their idols entice many idolaters to kill their
children, in order to lead them to their ruin, and confuse them in their
religion. Had God willed, they would not have done it; so leave them to
their fraud.

Quran 14:35 Recall that Abraham said, \"O my Lord, make this land
peaceful, and keep me and my sons from worshiping idols.\"

Quran 17:56 "Call on those whom you think to be gods beside Him; then
you will know that they have no power to remove affliction from you nor
to change your condition.'"

Quran 22:30 refrain from the filth of the idols and refrain from a word
of falsehood,

Quran 29:17 "You only worship idols beside Allah, and you invent a lie.
In fact those whom you worship beside Allah do not have power to give
you provision. So seek provision with Allah and worship Him and be
grateful to Him. To Him you are to be returned."

Quran 29:25 And he said, \"You have chosen idols instead of God, out of
affection for one another in the worldly life. But then, on the Day of
Resurrection, you will disown one another, and curse one another. Your
destiny is Hell, and you will have no saviors.\"

Quran 30:13 They will have no intercessors from among their idols, and
they will disown their partners.

Quran 46:28 Why then did the idols, whom they worshiped as means of
nearness to God, not help them? In fact, they abandoned them. It was
their lie, a fabrication of their own making.

God is the healer
-----------------

Quran 26:80 And when I get sick, He heals me.

Quran 2:155-157 155. We will certainly test you with some fear and
hunger, and some loss of possessions and lives and crops. But give good
news to the steadfast. 156. Those who, when a calamity afflicts them,
say, "To God we belong, and to Him we will return." 157. Upon these are
blessings and mercy from their Lord. These are the guided ones.

Quran 2:263 "Kind speech and forgiveness are better than charity
followed by injury." giving people a mask, which makes them suffocate is
charity followed by injury. Let us forgive and be kind to one another
instead.

Quran 3:186 You will be tested through your possessions and your
persons; and you will hear from those who received the Scripture before
you, and from the idol worshipers, much abuse. But if you persevere and
lead a righteous life---that indeed is a mark of great determination.

Quran 10:57 O people! There has come to you advice from your Lord, and
healing for what is in the hearts, and guidance and mercy for the
believers.

Quran 39:17 As for those who avoid the worship of idols, and devote
themselves to God---theirs is the good news. So give good news to My
servants.

Quran 41:44 Had We made it a Quran in a foreign language, they would
have said, \"If only its verses were made clear.\" Non-Arabic and an
Arab? Say, \"For those who believe, it is guidance and healing. But as
for those who do not believe: there is heaviness in their ears, and it
is blindness for them. These are being called from a distant place.\"

New Age/Science
===============

The Age of Peace has come (as of 2012) and the first step to entering in
the New Age Sylvan Timeline is forgiveness, compassion and unconditional
love for all beings. The vision of the new paradise is food forest
communities where you can meet all your needs on site. With co-creation
centres where you can make anything that you can imagine. We are
co-creating such 1000 generation sustainable communities in your area.

Documented in the book "Mass Dreams of the Future" In the 1980's
scientists future progressed 2,500 people to the years 2100-2500CE.
While there was a 94% depopulation by the year 2100CE, there were three
main timelines or ways of being on Earth.

One of the timelines the scientists called "high-tech urban" where
people lived underground wearing jumpsuits i.e. prison uniforms, they
lived under tyrannical control by a socialist government, and if anyone
rebelled they were sent outside in a leaky space-suit where they died of
asphyxiation as these bases were surrounded by poison gas, their average
life expectancy was 56 years. This is what you could expect to pass onto
your children if you cultivate fear/anger/control and worship of worldly
authority i.e. socialism.

Another timeline called "Rustic" people lived in the way of the Amish,
it was a hard life due to annual plowing agriculture, their life
expectancy was 59 years. This is what you could expect to pass onto your
children if you cling onto the past and dogmatic ways of being that
ignore God's newer teachings.

The largest timeline in terms of membership called "'New Age Sylvan''
people lived in spiritual communities set in food forests, where they
had access to technology and trade with the galactic community. The
average life expectancy was 99 years, and they had the highest
subjective ratings of well being.

LLResearch 2020/02/08 "Austin: Our friend S. from China, wrote recently
and he had a friend who asked him to relay a question to us. And it
reads:

"Now, there is an outbreak of the novel coronavirus in China, and it has
caused lots of worries, rumors, separations among people, as this
coronavirus outbreak partially resembles the SARS outbreak in China in
2002 to 2003. Q'uo, without infringing upon the free will and providing
your point of view, could you indicate the origin of the novel
coronavirus? Is the coronavirus man-made, as Q'uo indicated for SARS?
And whether it is man-made or not, what's the metaphysical meaning
behind this coronavirus outbreak? Does this kind of collective catalyst
also reflect the dysfunctional, unhealthy and pathological aspects of
our current social system, just as individual physical distortion
reflects catalyst unused by the mind complex?"

Q'uo: I am Q'uo, and am aware of the query, my brother. This is a
subject which we have indeed covered before. For various outbreaks of
this nature are attempts by what you may call the hidden powers to
control the population of the planet. For your planet is very heavily
populated at this time. And it is easier for those who seek control to
control fewer entities. Thusly, there is the manufacture of various
types of diseases that have been accomplished over the past few decades
with the goal of reducing the population of the planet.

The entities so involved in this experiencing of the coronavirus are
entities which have preincarnatively offered themselves in service to
the planetary mind, in order that there may be a resolution or
completion of certain cycles of vibration, that is to say, that there
may be the realization of their ability to serve their fellow human
beings by becoming infected in a fashion which reflects the need to find
a cure for this particular virus. This is a manner of being which each
entity undertook in order to become more able to open their own hearts
in love and compassion for others. For as they find themselves afflicted
with this particular virus, they become more and more compassionate for
their fellow humans who also have this virus within their being and must
suffer the consequences. Thus it is a way of, shall we say, utilizing a
negative initiative in a positive fashion that was foreseen before the
incarnation began.

There are many such possibility/probability vortices that have been and
are possible within your third-density illusion at this time. For the
harvest time is a time of great upheaval and change. There is much
volatility amongst many nations and individuals and groupings within
nations that makes it necessary for the type of experience that is now
being felt to be assessed in a manner which does not bring fear.
However, most entities are subject to the fear aspect of such an
outbreak of a virus of this nature. There is, in such an experience, the
opportunity to see that the Creator is knowing itself in all that
happens around one and within one.

When this type of attitude can be taken, then the negative efforts to
control the population in one manner or another may be transmuted
alchemically, individually, for each entity so able to do so in a manner
which sees the planetary game as that which is played upon the world
stage, in a manner that can offer an entity a great variety of
responses.

If the entity can choose the positive vision of the Creator experiencing
Itself, then it draws unto itself the basic nature of the power of the
truth of unity that is, that all is one, and that though one may pass
from this life, there is no loss. The One still remains in each entity
and in each endeavor, so that there is always the knowledge that the One
who exists in all is always there experiencing this event in a manner
which informs the Creator more and more of the nature of Itself.

Is there a further query, my brother?

Austin: Yes, S. himself sent a follow-up to the question, and I think
that you just touched on this. But I'd like to read it just in case
there's any more that you could say. S. wrote:

"Ra mentioned in 34.7 that 'These so-called contagious diseases are
those entities of second density which offer an opportunity for this
type of catalyst. If this catalyst is unneeded, then these
second-density creatures, as you would call them, do not have an effect.
In each of these generalizations you may please note that there are
anomalies so that we cannot speak to every circumstance but only to the
general run or way of things as you experience them.' And S. continues:
So it seems that in cases of anomalies, even if the catalyst is
unneeded, these second-density creatures can still have an effect. I
just wonder if SARS is one case of anomalies, since SARS can be regarded
as a biological weapon, according to Q'uo. If so, is it always possible
for those infected with any man-made virus such as SARS to nullify its
effects and heal themselves? I ask this question just for the purpose of
encouraging hope and faith in these cases of anomalies."

Q'uo: I am Q'uo, and am aware of your query, my brother. We would agree
that for the conscious seeker of truth who finds the spiritual path to
the One to be the only path worth traveling, this type of virus can be
seen as a mere rock upon the path that may be avoided by seeing the One
in all and loving the One in all, no matter what is the action of any
upon one, as attempts are made to control one. If one can give love
without expectation of return, and resist not evil, then one has a power
over evil which cannot be broken. It is the power of love, the power to
cure all that is unwell, to make whole all that is broken, and to bring
to light all that is hidden."

Canada's Constitution
=====================

Department of Justice, Charterpedia: Section 2(a), Freedom of Religion,
i. Freedom from conformity to religious dogma:"Government may not coerce
individuals into affirming a specific religious belief nor to manifest a
specific religious practice for a sectarian purpose."

Masks are a Faucist ritual of sectarian purpose to divide faucists from
non faucists.

Charterpedia, Freedom of Religion. Section 1: "measures that undermine
the character of lawful religious institutions and disrupt the vitality
of religious communities represent a profound interference with
religious freedom"

Charterpedia, Freedom of Religion, (ii) Secularism and state neutrality:
"A breach of a duty of state neutrality must be established by proving
that the state is professing, adopting or favouring one belief to the
exclusion of all others and that the exclusion has resulted in
interference with the complainant's freedom of conscience or religion ,"
https://www.justice.gc.ca/eng/csj-sjc/rfc-dlc/ccrf-ccdl/check/art2a.html

State breached neutrality by promoting the fear-anger based idol of
COVID, they preach COVID sermons 24/7 on government funded media, as via
over 600 million in media bailouts. There is a plan to rob Canadians of
all assets that has been leaked:
https://thecanadianreport.ca/is-this-leaked-memo-really-trudeaus-covid-plan-for-2021-you-decide/

Masks are a ritual object of Faucist Humanist Materialism as a ward
against death which they believe is caused by microbeings -- instead of
having the understanding that we are all spiritual being in a physical
illusion where physical problems are manifestations of unaddressed basic
needs and-or spiritual issues. Noting that the Qing Dynasty (empire)
spread by forcing people to have an outward display of conformity under
threat of lethal consequences. Empires only work because fear/anger
interferes with cognitive function. Similarly the CCP puts people who
have a faith other than the state religion of Humanist Materialism into
re-education camps where they may be tortured to death and have their
organs sold off. I forgive them but I can not in good conscience be an
accessory to a fear/anger/control hierarchy on Earth so do not practice
fear/anger-motivated rituals (such as mask mandates, wars, genocide
etc). Additionally masking is used in a sectarian us vs them way by some
people.

Charterpedia: "Section 2(c) includes the right to participate in
peaceful demonstrations, protests, parades, meetings, picketing and
other assemblies."

Re-opening Ontario Act
======================

Under the Ontario Re-opening Act, Rules for Stage One , and Indeed rules
for any stage of lockdown or otherwise, under Schedule 1 section 2.4.
requires the business owners to ensure people are wearing a mask "unless
the person in the indoor area, ", k) "is being reasonably accommodated
in accordance with the Human Rights Code;"

If one is not accommodated, then the business or place that is refusing
to serve could be fined up to the maximum fine under the Ontario
Re-opening Act. And the Ontario Human Rights Code says in Part I section
1 "Services1 Every person has a right to equal treatment with respect to
services, goods and facilities, without discrimination because of race,
ancestry, place of origin, colour, ethnic origin, citizenship, creed,
sex, sexual orientation, gender identity, gender expression, age,
marital status, family status or disability. "

Additionally all Canada Charter Rights can be enforced using the
Reopening Ontario Act, including freedom of religion.

Reopening Ontario Act: "continued section 7.0.2 order" means an order
continued under section 2 that was made under section 7.0.2 of the
Emergency Management and Civil Protection Act; (

Emergency Management and Civil Protection Act: 7.0.2 (1) The purpose of
making orders under this section is to promote the public good by
protecting the health, safety and welfare of the people of Ontario in
times of declared emergencies in a manner that is subject to the
Canadian Charter of Rights and Freedoms.  2006, c. 13, s. 1 (4).

Reopening Ontario Act: "Offences 10 (1) Every person who fails to comply
with subsection 9.1 (2) or (3) or with a continued section 7.0.2 order
or who interferes with or obstructs any person in the exercise of a
power or the performance of a duty conferred by such an order is guilty
of an offence and is liable on conviction, (a) in the case of an
individual, subject to clause (b), to a fine of not more than \$100,000
and for a term of imprisonment of not more than one year; (b) in the
case of an individual who is a director or officer of a corporation, to
a fine of not more than \$500,000 and for a term of imprisonment of not
more than one year; and (c) in the case of a corporation, to a fine of
not more than \$10,000,000. 2020, c. 17, s. 10 (1); 2020, c. 23, Sched.
6, s. 3."

So as you can see failing to comply with continued 7.0.2 (Canadian
Charter of Rights and Freedoms) or who obstructs any person using their
charter rights (powers) is guilty of an offence and is liable for a fine
up to \$100,000 or a term of imprisonment of up to a year.

Canadian Human Rights Act
=========================

3 (1) For all purposes of this Act, the prohibited grounds of
discrimination are race, national or ethnic origin, colour, religion,
age, sex, sexual orientation, gender identity or expression, marital
status, family status, genetic characteristics, disability and
conviction for an offence for which a pardon has been granted or in
respect of which a record suspension has been ordered.

5 It is a discriminatory practice in the provision of goods, services,
facilities or accommodation customarily available to the general public
(a) to deny, or to deny access to, any such good, service, facility or
accommodation to any individual, or (b) to differentiate adversely in
relation to any individual, on a prohibited ground of discrimination.

Conclusion
==========

I hope you see that it is fully lawful for one to worship God only, and
not the false idol of Fauci and his dictates.

Glorify in the Love and in the Light of the One Infinite Creator. Go
forth in the Power and in the Peace of the One Infinite Creator.
